//Write a program to find the sum of n different numbers using 4 functions
#include <stdio.h>
float input();
float sum(float a, float b);
float output(float c);
int main()
{
int n,i;
printf("enter the no. of term to calculate the sum: ");
scanf("%d",&n);
float a[n];
for(i=0;i<n;i++)
{
printf("Enter %d number: ",i+1);
a[i]=input();
}
float b = 0;
for(i=0;i<n;i++)
{
b = sum(b,a[i]);
}
output(b);
return 0;
}
float input(){
float v;
scanf("%f",&v);
return v;
}
float sum(float a, float b){
return (a+b);
}
float output(float c)
{
printf("the sum of the given no.: %f",c);
return 0;
}

