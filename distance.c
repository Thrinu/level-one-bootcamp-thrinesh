//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
struct points
{
float x, y;
};
float distance(struct points a, struct points b )
{
float result ;
result = sqrt((a.x - b.x)*(a.x-b.x)*(a.y-b.y)*(a.y-b.y));
return result ;
}
int main()
{
struct points a, b;
printf("Enter the coordinates of point A");
printf("x-axis coordinate");
scanf("%f",&a.x);
printf("Y - axis Coordinate");
scanf("%f" , &a.y);
printf("Enter the coordinates of point B");
printf("x - axis coordinate");
scanf("%f",&b.x);
printf("y - axis coordinate");
scanf("%f",&b.y);
printf("Distance between points A and B :%f" , distance(a,b));
return 0 ;
}

