//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>
struct coordinate
{
float x;
float y;
};
typedef struct coordinate point;
point input();
float distance(point p1, point p2);
float output(float dist);
int main() 
{
point p1, p2;
float d;
printf("Enter the coordinate of 1st point");
p1 = input();
printf("Enter the coordinate of 2nd point");
p2 = input();
d = distance(p1,p2);
output(d);
return 0;
}
point input()
{
point p;
printf("Enter x coordinate");
scanf("%f",&p.x);
printf("Enter y coordinate");
scanf("%f",&p.y);
return p;
}
float distance(point p1, point p2)
{
return (sqrt(((p2.x-p1.x)*(p2.x-p1.x))+((p2.y-p1.y)*(p2.y-p1.y))));
}
float output(float dist){
printf("The distance between two points is:%f",dist);
return 0;
}


