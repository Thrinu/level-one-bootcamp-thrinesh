//Write a program to add two user input numbers using 4 functions.
int getA()
{
    int a;
    printf("enter the value of a:\n");
    scanf("%d", &a);
    return a;
}
int getB()
{
    int b;
    printf("enter the value of b\n");
    scanf("%d", &b);
    return b;
}
int sum(int a , int b)
{
    int res ;
    res = a + b;
    return res;
}
int main()
{
    int a,b;
    float result;
    a=getA();
    b=getB();
    result=sum(a,b);
    printf("The result of the sum is : %f" , result);
    return 0;
}



